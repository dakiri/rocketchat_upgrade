Mise à jour d'une instance rocketchat (version docker) dans la version "rocket_chat_new_version"

1. Récupération de la version actuellement lancée via API
2. Création d'un dump de la base "{{ backup_root }}/dump_{{ rocketchat_version }}_{{ dump_time }}.tgz"
3. Sauvegarde du docker-compose
4. Actualisation du docker-compose : rocketchat/rocket.chat:{{ rocket_chat_new_version }}
5. Récupération de l'image (docker pull)
6. Rédémarrage des services

Prérequis :

```
apt-get install python-dev --no-install-recommends
pip install wheel
pip install setuptools
pip install docker-compose
pip install docker
```